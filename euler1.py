 # O(N)   
def primality(n):
  if (n > 1):
    for i in range((n - 1),1,-1):
        if(n % i == 0):
            return 0
    return 1

def smallest_multiple(n):
  nums = []
  primes = []

  # Populate with the numbers
  for i in range(n):
    nums.append(i + 1)

  factors = []

  for i in reversed(nums):
    primes = []
    result = 1
    for j in range(i):
      # It's a factor and prime
      if (i % (j + 1) == 0) and primality(j + 1):
        num_times = 1 
        while(result <= i) and ((i % ((j + 1 ) ** num_times)) == 0):
            result *= (j+1)
            num_times += 1
            primes.append(1 + j)
    factors.append((i,primes))

  occurances = []
  temp       = []  

  for i in range(n):
    occurances.append(0)
    temp.append(0)

  for i in factors:
    for l in range(n):
      temp[l] = 0 
    for j in i[1]:
      temp[j] = temp[j] + 1
    for k in range(n):
      if temp[k] > occurances[k]:
        occurances[k] = temp[k]  

  small_mul = 1 
  for i in range(n):
    if occurances[i] > 0:
      small_mul  = i  ** occurances[i] * small_mul
  print(small_mul)

def sum_square(n):
  sum = (n * (n + 1) * (2 * n + 1))/6
  result = (n/2) * ( 1 + n)
  result = result ** 2
  print(result - sum)


def read_file(input):
  with open(input) as file:
    data = file.read().split(",")
    data.sort()
    return data

def values(data):
  entries = []
  for name in data:
    value = 0
    for letter in name:
      value += letter - 64
    entry = (name,value)
    entries.append(entry)
  return entries



def main():
   data = read_file("names.txt")
   print(values(data))
if __name__== "__main__":
    main()